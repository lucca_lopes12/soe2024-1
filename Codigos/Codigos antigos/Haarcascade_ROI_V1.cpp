#include <opencv2/opencv.hpp>
#include <opencv2/objdetect.hpp>
#include <pthread.h>
#include <iostream>
#include <vector>
#include <mutex>

using namespace cv;
using namespace std;

// Variáveis globais e mutex para sincronização
Mat frame;
pthread_mutex_t frame_mutex = PTHREAD_MUTEX_INITIALIZER;
bool running = true;

// Estrutura para passar múltiplos argumentos para threads
struct ThreadData {
    VideoCapture* cap;
    CascadeClassifier* face_cascade;
    Rect leftROI;
    Rect rightROI;
};

// Função para capturar frames da webcam
void* captureFrames(void* arg) {
    VideoCapture* cap = (VideoCapture*)arg;
    while (running) {
        Mat temp_frame;
        *cap >> temp_frame;
        if (temp_frame.empty()) continue;
        pthread_mutex_lock(&frame_mutex);
        frame = temp_frame.clone();
        pthread_mutex_unlock(&frame_mutex);
    }
    return nullptr;
}

// Função para processar frames (detecção facial)
void* processFrames(void* arg) {
    ThreadData* data = (ThreadData*)arg;
    CascadeClassifier* face_cascade = data->face_cascade;
    Rect leftROI = data->leftROI;
    Rect rightROI = data->rightROI;

    while (running) {
        Mat local_frame;
        pthread_mutex_lock(&frame_mutex);
        if (frame.empty()) {
            pthread_mutex_unlock(&frame_mutex);
            continue;
        }
        local_frame = frame.clone();
        pthread_mutex_unlock(&frame_mutex);

        // Converter para escala de cinza para detecção facial
        Mat gray;
        cvtColor(local_frame, gray, COLOR_BGR2GRAY);
        equalizeHist(gray, gray);

        // Detectar faces
        vector<Rect> faces;
        face_cascade->detectMultiScale(gray, faces);

        // Desenhar retângulos nas regiões de interesse
        rectangle(local_frame, leftROI, Scalar(0, 255, 0), 2);
        rectangle(local_frame, rightROI, Scalar(0, 0, 255), 2);

        // Verificar se há faces na região da direita e desenhar retângulos nas faces detectadas
        for (size_t i = 0; i < faces.size(); i++) {
            Rect face = faces[i];
            Point center(face.x + face.width / 2, face.y + face.height / 2);
            ellipse(local_frame, center, Size(face.width / 2, face.height / 2), 0, 0, 360, Scalar(255, 0, 255), 2);

            // Verificar se a face está na região da direita
            if (face.x >= rightROI.x && face.x + face.width <= rightROI.x + rightROI.width) {
                cout << "Alerta" << endl;
            }
        }

        // Mostrar a imagem completa com os retângulos das ROIs e as faces detectadas
        imshow("Webcam with Face Detection", local_frame);

        // Verificar se a tecla 'q' foi pressionada para sair do loop
        if (waitKey(1) == 'q') {
            running = false;
            break;
        }
    }
    return nullptr;
}

int main() {
    // Inicializar a webcam
    VideoCapture cap(0);
    if (!cap.isOpened()) {
        cerr << "Erro ao abrir a webcam" << endl;
        return -1;
    }

    // Carregar o classificador Haar para detecção facial
    CascadeClassifier face_cascade;
    if (!face_cascade.load("haarcascade_frontalface_default.xml")) {
        cout << "Erro ao carregar o classificador Haar." << endl;
        return -1;
    }

    // Definir as dimensões do frame capturado pela webcam
    int frame_width = cap.get(CAP_PROP_FRAME_WIDTH);
    int frame_height = cap.get(CAP_PROP_FRAME_HEIGHT);
    Rect leftROI(0, 0, frame_width / 2, frame_height);
    Rect rightROI(frame_width / 2, 0, frame_width / 2, frame_height);

    // Estrutura para passar os dados para as threads
    ThreadData data;
    data.cap = &cap;
    data.face_cascade = &face_cascade;
    data.leftROI = leftROI;
    data.rightROI = rightROI;

    // Criar threads para captura e processamento
    pthread_t capture_thread, processing_thread;
    pthread_create(&capture_thread, nullptr, captureFrames, &cap);
    pthread_create(&processing_thread, nullptr, processFrames, &data);

    // Esperar pelas threads
    pthread_join(capture_thread, nullptr);
    pthread_join(processing_thread, nullptr);

    // Liberar recursos
    cap.release();
    destroyAllWindows();
    pthread_mutex_destroy(&frame_mutex);

    return 0;
}
