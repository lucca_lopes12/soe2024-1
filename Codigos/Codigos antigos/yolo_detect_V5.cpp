#include <opencv2/dnn.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <fstream>
#include <thread>
#include <ctime>
#include <iomanip>
#include <sstream>
#include <chrono>
#include <filesystem>
#include <mutex>                 // Inclui funções para exclusão mútua
#include <condition_variable>    // Inclui funções para variáveis de condição

namespace fs = std::filesystem;

// Função para carregar os nomes das classes
std::vector<std::string> loadClassNames(const std::string& filename) {
    std::vector<std::string> classNames;
    std::ifstream file(filename);
    if (!file.is_open()) {
        std::cerr << "Erro ao abrir o arquivo de nomes: " << filename << std::endl;
        return classNames;
    }
    std::string line;
    while (std::getline(file, line)) {
        classNames.push_back(line);
    }
    return classNames;
}

// Função para obter a data atual formatada como "Foto i hh-min-dd-mm-aaaa"
std::string getCurrentDateFormatted(int photoCount) {
    std::time_t now = std::time(nullptr);
    std::tm* localTime = std::localtime(&now);
    std::ostringstream oss;
    oss << "Foto " << photoCount << " "
        << std::put_time(localTime, "%H-%M-%d-%m-%Y");
    return oss.str();
}

// Variáveis globais compartilhadas entre as threads
cv::Mat frame;
std::mutex frameMutex;
std::condition_variable frameCondVar;
bool frameReady = false;
bool running = true;
int photoCount = 0; // Contador de fotos

void captureFrames(cv::VideoCapture& cap) {
    while (running) {
        cv::Mat newFrame;
        cap >> newFrame;
        if (newFrame.empty()) {
            std::cerr << "Erro ao capturar frame" << std::endl;
            continue;
        }

        std::lock_guard<std::mutex> lock(frameMutex);
        frame = newFrame.clone();
        frameReady = true;
        frameCondVar.notify_one();
    }
}

void processFrames(cv::dnn::Net& net, const std::vector<std::string>& classNames, cv::Rect& leftROI, cv::Rect& rightROI) {
    while (running) {
        std::unique_lock<std::mutex> lock(frameMutex);
        frameCondVar.wait(lock, [] { return frameReady; });
        cv::Mat currentFrame = frame.clone();
        frameReady = false;
        lock.unlock();

        // Criar um blob a partir do frame
        cv::Mat blob = cv::dnn::blobFromImage(currentFrame, 1 / 255.0, cv::Size(416, 416), cv::Scalar(0, 0, 0), true, false);
        net.setInput(blob);

        // Obter as detecções da rede
        std::vector<cv::Mat> outs;
        net.forward(outs, net.getUnconnectedOutLayersNames());

        bool personInRightROI = false;
        bool personInLeftROI = false;

        // Processar as detecções
        for (const auto& detection : outs) {
            for (int i = 0; i < detection.rows; ++i) {
                const int probability_index = 5;
                const int size = detection.cols - probability_index;
                float* data = (float*)detection.data + i * detection.cols;
                float confidence = data[4];

                if (confidence > 0.5) {
                    cv::Mat scores(1, size, CV_32FC1, data + probability_index);
                    cv::Point classIdPoint;
                    double maxClassScore;
                    minMaxLoc(scores, 0, &maxClassScore, 0, &classIdPoint);

                    if (classNames[classIdPoint.x] == "person" && maxClassScore > 0.5) {
                        int centerX = (int)(data[0] * currentFrame.cols);
                        int centerY = (int)(data[1] * currentFrame.rows);
                        int width = (int)(data[2] * currentFrame.cols);
                        int height = (int)(data[3] * currentFrame.rows);
                        int left = centerX - width / 2;
                        int top = centerY - height / 2;

                        if (rightROI.contains(cv::Point(centerX, centerY))) {
                            personInRightROI = true;
                        }

                        if (leftROI.contains(cv::Point(centerX, centerY))) {
                            personInLeftROI = true;
                        }

                        // Desenhar a caixa delimitadora
                        cv::rectangle(currentFrame, cv::Rect(left, top, width, height), cv::Scalar(0, 255, 0), 2);
                        std::string label = cv::format("%.2f", confidence);
                        label = classNames[classIdPoint.x] + ":" + label;
                        int baseLine;
                        cv::Size labelSize = cv::getTextSize(label, cv::FONT_HERSHEY_SIMPLEX, 0.5, 1, &baseLine);
                        top = std::max(top, labelSize.height);
                        cv::putText(currentFrame, label, cv::Point(left, top - 4), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0, 255, 0), 1);
                    }
                }
            }
        }

        // Desenhar retângulos nas regiões de interesse e escreve o que significa cada area
        cv::rectangle(currentFrame, leftROI, cv::Scalar(0, 255, 0), 2);
        cv::rectangle(currentFrame, rightROI, cv::Scalar(0, 0, 255), 2);
        cv::putText(currentFrame, "Area da cama", cv::Point(10, 60), cv::FONT_HERSHEY_SIMPLEX, 1.0, cv::Scalar(0, 255, 0), 2);
        cv::putText(currentFrame, "Area de queda", cv::Point(rightROI.x + 10, rightROI.y + 60), cv::FONT_HERSHEY_SIMPLEX, 1.0, cv::Scalar(0, 0, 255), 2);

        if (personInRightROI) {
            std::cout << "Alerta: Pessoa detectada na região de queda" << std::endl;

            // Incrementa o contador de fotos
            photoCount++;
            std::string dateStr = getCurrentDateFormatted(photoCount);
            std::string filename = "fotos/" + dateStr + ".jpg";
            fs::create_directories("fotos");

            if (!cv::imwrite(filename, currentFrame)) {
                std::cerr << "Erro ao salvar a imagem." << std::endl;
            } else {
                std::cout << "Imagem salva em: " << filename << std::endl;

                // Entrar em repouso por 30 segundos antes de apagar a imagem
                std::this_thread::sleep_for(std::chrono::seconds(30));

                if (fs::remove(filename)) {
                    std::cout << "Imagem apagada: " << filename << std::endl;
                } else {
                    std::cerr << "Erro ao apagar a imagem." << std::endl;
                }
            }
        }

        if (personInLeftROI) {
            std::cout << "Paciente está na cama" << std::endl;
        }

//        cv::imshow("Monitoramento de Paciente", currentFrame);

        if (cv::waitKey(1) == 'q') {
            running = false;
            frameCondVar.notify_all();
            break;
        }
    }
}

int main() {
    // Carregar a rede neural YOLOv4 Tiny
    cv::dnn::Net net = cv::dnn::readNet("yolov4-tiny.weights", "yolov4-tiny.cfg", "Darknet");

    net.setPreferableBackend(cv::dnn::DNN_BACKEND_DEFAULT);
    net.setPreferableTarget(cv::dnn::DNN_TARGET_CPU);

    // Carregar os nomes das classes
    std::vector<std::string> classNames = loadClassNames("coco.names");
    if (classNames.empty()) {
        return -1;
    }

    // Abrir a webcam
    cv::VideoCapture cap(0);
    if (!cap.isOpened()) {
        std::cerr << "Erro ao abrir a webcam" << std::endl;
        return -1;
    }

    int frame_width = cap.get(cv::CAP_PROP_FRAME_WIDTH);
    int frame_height = cap.get(cv::CAP_PROP_FRAME_HEIGHT);

    cv::Rect leftROI(0, 0, frame_width / 2, frame_height);
    cv::Rect rightROI(frame_width / 2, 0, frame_width / 2, frame_height);

    // Iniciar as threads de captura e processamento
    std::thread captureThread(captureFrames, std::ref(cap));
    std::thread processingThread(processFrames, std::ref(net), std::ref(classNames), std::ref(leftROI), std::ref(rightROI));

    captureThread.join();
    processingThread.join();

    cap.release();
    cv::destroyAllWindows();

    return 0;
}
