#include <stdio.h>     // Inclui a biblioteca stdio para operações de entrada/saída
#include <stdlib.h>    // Inclui a biblioteca stdlib para funções como exit()
#include <signal.h>    // Inclui a biblioteca signal para lidar com sinais, como o Ctrl+C
#include <string.h>    // Inclui a biblioteca string para manipulação de strings

// Constantes para o token do bot e ID do chat do Telegram
#define TELEGRAM_BOT_TOKEN "7036557642:AAHmTeXHB84BeYMUnic8ERz2RrXBcSu_QJQ"
#define CHAT_ID "2122899156"

// Função para enviar uma mensagem ao chat do Telegram
void send_telegram_message(const char *message) {
    char command[512];
    snprintf(command, sizeof(command), "curl -s -X POST https://api.telegram.org/bot%s/sendMessage -d chat_id=%s -d text=\"%s\"", TELEGRAM_BOT_TOKEN, CHAT_ID, message);
    system(command);
}

// Função para lidar com o sinal SIGINT (Ctrl+C)
void signal_handler(int signum) {
    printf("Encerrando o programa...\n");
    exit(signum);
}

int main() {
    // Configura o manipulador de sinais para lidar com o sinal SIGINT (Ctrl+C)
    signal(SIGINT, signal_handler);

    // Envia uma mensagem para o Telegram ao iniciar o programa
    send_telegram_message("Algo aconteceu!");

    // Mantém o programa em execução indefinidamente
    while (1) {
        // Pode adicionar aqui outras operações ou simplesmente aguardar
    }

    return 0;
}
