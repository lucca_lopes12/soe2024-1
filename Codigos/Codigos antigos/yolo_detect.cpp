#include <opencv2/dnn.hpp>       // Inclui o módulo Deep Neural Network (DNN) do OpenCV
#include <opencv2/highgui.hpp>   // Inclui funções para interface gráfica e manipulação de janelas
#include <opencv2/imgproc.hpp>   // Inclui funções para processamento de imagem
#include <iostream>              // Inclui funções de entrada e saída padrão
#include <fstream>               // Inclui funções para manipulação de arquivos
#include <chrono>                // Inclui funções para medição de tempo

// Função para carregar os nomes das classes
std::vector<std::string> loadClassNames(const std::string& filename) {
    std::vector<std::string> classNames;
    std::ifstream file(filename);  // Abre o arquivo de nomes das classes
    if (!file.is_open()) {         // Verifica se o arquivo foi aberto com sucesso
        std::cerr << "Erro ao abrir o arquivo de nomes: " << filename << std::endl;
        return classNames;         // Retorna um vetor vazio em caso de erro
    }
    std::string line;
    while (std::getline(file, line)) {  // Lê cada linha do arquivo
        classNames.push_back(line);     // Adiciona o nome da classe ao vetor
    }
    return classNames;  // Retorna o vetor com os nomes das classes
}

// Função principal
int main() {
    // Carregar a rede neural YOLOv4 Tiny
    cv::dnn::Net net = cv::dnn::readNet("yolov4-tiny.weights", "yolov4-tiny.cfg", "Darknet");

    // Use CUDA (GPU) se disponível, caso contrário, use CPU
    net.setPreferableBackend(cv::dnn::DNN_BACKEND_DEFAULT);
    net.setPreferableTarget(cv::dnn::DNN_TARGET_CPU);

    // Carregar os nomes das classes
    std::vector<std::string> classNames = loadClassNames("coco.names");
    if (classNames.empty()) {  // Verifica se os nomes das classes foram carregados corretamente
        return -1;  // Sai do programa em caso de erro
    }

    // Abrir a câmera
    cv::VideoCapture cap(0);
    if (!cap.isOpened()) {  // Verifica se a câmera foi aberta com sucesso
        std::cerr << "Erro ao abrir a câmera" << std::endl;
        return -1;  // Sai do programa em caso de erro
    }

    // Variáveis para cálculo de FPS
    auto start = std::chrono::high_resolution_clock::now();
    int frameCount = 0;

    while (true) {
        cv::Mat frame;
        cap >> frame;  // Captura um frame da câmera
        if (frame.empty()) {  // Verifica se o frame foi capturado com sucesso
            std::cerr << "Erro ao capturar frame" << std::endl;
            break;  // Sai do loop em caso de erro
        }

        // Criar um blob a partir do frame
        cv::Mat blob = cv::dnn::blobFromImage(frame, 1 / 255.0, cv::Size(416, 416), cv::Scalar(0, 0, 0), true, false);

        // Definir o blob como entrada para a rede
        net.setInput(blob);

        // Obter as detecções da rede
        std::vector<cv::Mat> outs;
        net.forward(outs, net.getUnconnectedOutLayersNames());

        // Processar as detecções
        for (const auto& detection : outs) {
            for (int i = 0; i < detection.rows; ++i) {
                const int probability_index = 5;  // Índice onde começam as probabilidades das classes
                const int size = detection.cols - probability_index;
                float* data = (float*)detection.data + i * detection.cols;
                float confidence = data[4];  // Confiança da detecção

                if (confidence > 0.5) {  // Filtra detecções com confiança baixa
                    cv::Mat scores(1, size, CV_32FC1, data + probability_index);
                    cv::Point classIdPoint;
                    double maxClassScore;
                    minMaxLoc(scores, 0, &maxClassScore, 0, &classIdPoint);  // Encontra a classe com a maior pontuação

                    if (classNames[classIdPoint.x] == "person" && maxClassScore > 0.5) {  // Verifica se a classe é "person" e tem alta pontuação
                        int centerX = (int)(data[0] * frame.cols);
                        int centerY = (int)(data[1] * frame.rows);
                        int width = (int)(data[2] * frame.cols);
                        int height = (int)(data[3] * frame.rows);
                        int left = centerX - width / 2;
                        int top = centerY - height / 2;

                        // Desenhar a caixa delimitadora
                        cv::rectangle(frame, cv::Rect(left, top, width, height), cv::Scalar(0, 255, 0), 2);
                        std::string label = cv::format("%.2f", confidence);
                        label = classNames[classIdPoint.x] + ":" + label;
                        int baseLine;
                        cv::Size labelSize = cv::getTextSize(label, cv::FONT_HERSHEY_SIMPLEX, 0.5, 1, &baseLine);
                        top = std::max(top, labelSize.height);
                        cv::putText(frame, label, cv::Point(left, top - 4), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0, 255, 0), 1);
                    }
                }
            }
        }

        // Incrementar o contador de frames
        frameCount++;

        // Calcular o tempo decorrido e FPS
        auto end = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double> elapsed = end - start;
        if (elapsed.count() >= 1.0) {  // Atualiza o FPS a cada segundo
            double fps = frameCount / elapsed.count();
            frameCount = 0;
            start = end;

            // Exibir o FPS no canto superior esquerdo
            std::string fpsLabel = cv::format("FPS: %.2f", fps);
            cv::putText(frame, fpsLabel, cv::Point(10, 30), cv::FONT_HERSHEY_SIMPLEX, 1.0, cv::Scalar(0, 255, 0), 2);
        }

        // Mostrar o frame processado
        cv::imshow("YOLOv4 Tiny - Pessoas", frame);

        // Sair do loop se a tecla 'q' for pressionada
        if (cv::waitKey(1) == 'q') {
            break;
        }
    }

    return 0;  // Fim do programa
}
