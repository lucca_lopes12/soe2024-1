#include "telegram-funcs.h"
#include <iostream>
#include <tgbot/tgbot.h>
#include <fstream>
#include <sys/stat.h> // Para mkdir
#include <errno.h> // Para tratamento de erros do POSIX

// Sempre que uma nova mensagem chega a função processMessage é acionada
TelegramFuncs::TelegramFuncs(const std::string &token) : botToken(token)
{
    bot = new TgBot::Bot(token);
    bot->getEvents().onAnyMessage([this](TgBot::Message::Ptr message) {
        processMessage(message, *this);
    });
}

TelegramFuncs::~TelegramFuncs()
{
    delete bot;
}

TgBot::Bot* TelegramFuncs::getBot() const
{
    return bot;
}

// Função para enviar mensagens
void TelegramFuncs::sendMessage(const std::string &chatId, const std::string &text)
{
    bot->getApi().sendMessage(chatId, text);
}

// Retornar a ultima mensagem recebida
std::string TelegramFuncs::getLastMessage()
{
    return receivedMessage;
}

// Alterar o conteudo do buffer que guarda a ultima mensagem recebida
void TelegramFuncs::setMessage(std::string message)
{
    receivedMessage = message;
}

// Retornar o chat id da ultima mensagem recebida
std::string TelegramFuncs::getLastChatID()
{
    return chatID;
}

// Alterar o conteudo do buffer que guarda o id da ultima mensagem recebida
void TelegramFuncs::setChatID(int64_t id)
{
    chatID = std::to_string(id);
}

// Sempre que uma nova mensagem chega, printar na tela e salvar o chat id e a mensagem em buffers
void processMessage(TgBot::Message::Ptr message, TelegramFuncs& telegramFuncs)
{
    printf("Nova mensagem recebida de chat ID: %ld\n", message->chat->id);
    if (!message->text.empty()) {
        printf("Texto da mensagem: %s\n", message->text.c_str());
        telegramFuncs.setMessage(message->text.c_str());
    }
    telegramFuncs.setChatID(message->chat->id);
}
