#include <opencv2/dnn.hpp>       // Inclui o módulo Deep Neural Network (DNN) do OpenCV
#include <opencv2/highgui.hpp>   // Inclui funções para interface gráfica e manipulação de janelas
#include <opencv2/imgproc.hpp>   // Inclui funções para processamento de imagem
#include <opencv2/opencv.hpp>    // Inclui a biblioteca OpenCV
#include <iostream>              // Inclui funções de entrada e saída padrão
#include <fstream>               // Inclui funções para manipulação de arquivos
#include <chrono>                // Inclui funções para medição de tempo
#include <thread>                // Inclui funções para criação e manipulação de threads
#include <mutex>                 // Inclui funções para exclusão mútua
#include <condition_variable>    // Inclui funções para variáveis de condição

// Função para carregar os nomes das classes
std::vector<std::string> loadClassNames(const std::string& filename) {
    std::vector<std::string> classNames;
    std::ifstream file(filename);  // Abre o arquivo de nomes das classes
    if (!file.is_open()) {         // Verifica se o arquivo foi aberto com sucesso
        std::cerr << "Erro ao abrir o arquivo de nomes: " << filename << std::endl;
        return classNames;         // Retorna um vetor vazio em caso de erro
    }
    std::string line;
    while (std::getline(file, line)) {  // Lê cada linha do arquivo
        classNames.push_back(line);     // Adiciona o nome da classe ao vetor
    }
    return classNames;  // Retorna o vetor com os nomes das classes
}

// Variáveis globais compartilhadas entre as threads
cv::Mat frame;
std::mutex frameMutex;
std::condition_variable frameCondVar;
bool frameReady = false;
bool running = true;

void captureFrames(cv::VideoCapture& cap) {
    while (running) {
        cv::Mat newFrame;
        cap >> newFrame;  // Captura um frame da câmera
        if (newFrame.empty()) {  // Verifica se o frame foi capturado com sucesso
            std::cerr << "Erro ao capturar frame" << std::endl;
            continue;  // Continua para a próxima iteração do loop
        }

        std::lock_guard<std::mutex> lock(frameMutex);
        frame = newFrame.clone();
        frameReady = true;
        frameCondVar.notify_one();  // Notifica a thread de inferência de que um novo frame está disponível
    }
}

void processFrames(cv::dnn::Net& net, const std::vector<std::string>& classNames, cv::Rect& leftROI, cv::Rect& rightROI) {
    auto start = std::chrono::high_resolution_clock::now();
    int frameCount = 0;

    while (running) {
        std::unique_lock<std::mutex> lock(frameMutex);
        frameCondVar.wait(lock, [] { return frameReady; });
        cv::Mat currentFrame = frame.clone();
        frameReady = false;
        lock.unlock();

        // Criar um blob a partir do frame
        cv::Mat blob = cv::dnn::blobFromImage(currentFrame, 1 / 255.0, cv::Size(416, 416), cv::Scalar(0, 0, 0), true, false);
        net.setInput(blob);

        // Obter as detecções da rede
        std::vector<cv::Mat> outs;
        net.forward(outs, net.getUnconnectedOutLayersNames());

        bool personInRightROI = false;
        bool personInLeftROI = false;

        // Processar as detecções
        for (const auto& detection : outs) {
            for (int i = 0; i < detection.rows; ++i) {
                const int probability_index = 5;
                const int size = detection.cols - probability_index;
                float* data = (float*)detection.data + i * detection.cols;
                float confidence = data[4];

                if (confidence > 0.5) {
                    cv::Mat scores(1, size, CV_32FC1, data + probability_index);
                    cv::Point classIdPoint;
                    double maxClassScore;
                    minMaxLoc(scores, 0, &maxClassScore, 0, &classIdPoint);

                    if (classNames[classIdPoint.x] == "person" && maxClassScore > 0.5) {
                        int centerX = (int)(data[0] * currentFrame.cols);
                        int centerY = (int)(data[1] * currentFrame.rows);
                        int width = (int)(data[2] * currentFrame.cols);
                        int height = (int)(data[3] * currentFrame.rows);
                        int left = centerX - width / 2;
                        int top = centerY - height / 2;

                        if (rightROI.contains(cv::Point(centerX, centerY))) {
                            personInRightROI = true;
                        }

                        if (leftROI.contains(cv::Point(centerX, centerY))) {
                            personInLeftROI = true;
                        }

                        // Desenhar a caixa delimitadora
                        cv::rectangle(currentFrame, cv::Rect(left, top, width, height), cv::Scalar(0, 255, 0), 2);
                        std::string label = cv::format("%.2f", confidence);
                        label = classNames[classIdPoint.x] + ":" + label;
                        int baseLine;
                        cv::Size labelSize = cv::getTextSize(label, cv::FONT_HERSHEY_SIMPLEX, 0.5, 1, &baseLine);
                        top = std::max(top, labelSize.height);
                        cv::putText(currentFrame, label, cv::Point(left, top - 4), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0, 255, 0), 1);
                    }
                }
            }
        }

        // Incrementar o contador de frames
        frameCount++;

        // Calcular o tempo decorrido e FPS
        auto end = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double> elapsed = end - start;
        if (elapsed.count() >= 1.0) {
            double fps = frameCount / elapsed.count();
            frameCount = 0;
            start = end;

            // Exibir o FPS no canto superior esquerdo
            std::string fpsLabel = cv::format("FPS: %.2f", fps);
            cv::putText(currentFrame, fpsLabel, cv::Point(10, 30), cv::FONT_HERSHEY_SIMPLEX, 1.0, cv::Scalar(0, 255, 0), 2);
        }

        // Desenhar retângulos nas regiões de interesse
        cv::rectangle(currentFrame, leftROI, cv::Scalar(0, 255, 0), 2);
        cv::rectangle(currentFrame, rightROI, cv::Scalar(0, 0, 255), 2);

        if (personInRightROI) {
            std::cout << "Alerta: Pessoa detectada na região de queda" << std::endl;
        }

        if (personInLeftROI) {
	   std::cout << "Paciente esta na cama" << std::endl;
	}

        cv::imshow("YOLOv4 Tiny - Pessoas", currentFrame);

        if (cv::waitKey(1) == 'q') {
            running = false;
            frameCondVar.notify_all();
            break;
        }
    }
}

int main() {
    // Carregar a rede neural YOLOv4 Tiny
    cv::dnn::Net net = cv::dnn::readNet("yolov4-tiny.weights", "yolov4-tiny.cfg", "Darknet");

    // Use CUDA (GPU) se disponível, caso contrário, use CPU
    net.setPreferableBackend(cv::dnn::DNN_BACKEND_DEFAULT);
    net.setPreferableTarget(cv::dnn::DNN_TARGET_CPU);

    // Carregar os nomes das classes
    std::vector<std::string> classNames = loadClassNames("coco.names");
    if (classNames.empty()) {
        return -1;
    }

    // Abrir a webcam
    cv::VideoCapture cap(0);
    if (!cap.isOpened()) {
        std::cerr << "Erro ao abrir a webcam" << std::endl;
        return -1;
    }

    int frame_width = cap.get(cv::CAP_PROP_FRAME_WIDTH);
    int frame_height = cap.get(cv::CAP_PROP_FRAME_HEIGHT);

    cv::Rect leftROI(0, 0, frame_width / 2, frame_height);
    cv::Rect rightROI(frame_width / 2, 0, frame_width / 2, frame_height);

    // Iniciar as threads de captura e processamento
    std::thread captureThread(captureFrames, std::ref(cap));
    std::thread processingThread(processFrames, std::ref(net), std::ref(classNames), std::ref(leftROI), std::ref(rightROI));

    captureThread.join();
    processingThread.join();

    cap.release();
    cv::destroyAllWindows();

    return 0;
}
