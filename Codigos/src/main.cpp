#include "../include/telegram-funcs.h"
#include "../include/yolo_detect_V5.h"
#include <tgbot/tgbot.h>
#include <cstdio>
#include <csignal>
#include <unistd.h> 
#include <cstring>  
#include <iostream>
#include <ctime> 
#include"../include/secrets.h" // coloque seu define para o token --> #define TOKEN "codigo"

void messageCheck(int *pipe, pid_t pid_filho);
void photoAnalyze(int *pipe);

int main() {
    pid_t pid;   
    int fd[2];  
    pipe(fd); 
    pid = fork(); 
    if(pid == 0) {
        photoAnalyze(fd);
    } else {
        messageCheck(fd, pid);

    }
    return 0;
}

void messageCheck(int *pipe, pid_t pid_filho) {

    TelegramFuncs telegramBot(TOKEN);
    TgBot::TgLongPoll longPoll(*telegramBot.getBot(), 5, 1);
    bool startedFlag = false;
    while (true) {
        
        
        longPoll.start();
        
        std::string lastMessage = telegramBot.getLastMessage();

        if (!strcmp(telegramBot.getLastMessage().c_str(), "Iniciar") && !startedFlag) {
            telegramBot.sendMessage(telegramBot.getLastChatID(), "Iniciando envio de fotos");
            telegramBot.setMessage("");
            startedFlag = true;
            printf("Iniciando envio de fotos\n");
        } else if (!strcmp(telegramBot.getLastMessage().c_str(), "Encerrar") && startedFlag) {   
            startedFlag = false;
            telegramBot.sendMessage(telegramBot.getLastChatID(), "Encerrando programa");
            telegramBot.setMessage("");
            printf("Encerrando o programa\n");
            kill(pid_filho, SIGKILL);
            return;
        }  

        if (startedFlag) { 

            char filename[64];
            int bytesRead = read(pipe[0], filename, sizeof(filename));
            printf("Valor de %d\n", bytesRead);
            if(bytesRead > 0) {

                printf("%s\n", filename);
                telegramBot.sendPhoto(telegramBot.getLastChatID(), filename);
                telegramBot.sendMessage(telegramBot.getLastChatID(), "O paciente caiu! Favor iniciar atendimento");
                telegramBot.setMessage("");
            }
            
        }  

    }
}
void photoAnalyze(int *pipe) {

    // Carregar a rede neural YOLOv4 Tiny
    cv::dnn::Net net = cv::dnn::readNet("./yolo-files/yolov4-tiny.weights", "./yolo-files/yolov4-tiny.cfg", "Darknet");

    net.setPreferableBackend(cv::dnn::DNN_BACKEND_DEFAULT);
    net.setPreferableTarget(cv::dnn::DNN_TARGET_CPU);

    // Carregar os nomes das classes
    std::vector<std::string> classNames = loadClassNames("./yolo-files/coco.names");
    if (classNames.empty()) {
        return;
    }

    // Abrir a webcam
    cv::VideoCapture cap(-1);
//    cv::VideoCapture cap(0, cv::CAP_V4L);
    if (!cap.isOpened()) {
        std::cerr << "Erro ao abrir a webcam" << std::endl;
        return;
    }

    int frame_width = cap.get(cv::CAP_PROP_FRAME_WIDTH);
    int frame_height = cap.get(cv::CAP_PROP_FRAME_HEIGHT);

    cv::Rect leftROI(0, 0, frame_width / 2, frame_height);
    cv::Rect rightROI(frame_width / 2, 0, frame_width / 2, frame_height);


    // Iniciar as threads de captura e processamento
    std::thread captureThread(captureFrames, std::ref(cap));
    std::thread processingThread(processFrames, std::ref(net), std::ref(classNames), std::ref(leftROI), std::ref(rightROI), std::ref(pipe));

    captureThread.join();
    processingThread.join();

    cap.release();
    cv::destroyAllWindows();

    return;
}
