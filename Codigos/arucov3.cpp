#include <opencv2/aruco.hpp>
#include <iostream>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

int main()
{
    cv::Mat imgIn;
    cv::Ptr<cv::aruco::Dictionary> dict = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_4X4_50);
    cv::namedWindow("Out", 0);

    cv::VideoCapture capIn(-1); // or your camera id

    while (1)
    {
        capIn >> imgIn;
        if (imgIn.empty()) {
            std::cout << "Empty frame, ending loop." << std::endl;
            break;
        }

        cv::Mat frame = imgIn.clone();

        std::vector<std::vector<cv::Point2f>> corners;
        std::vector<int> ids;

        cv::aruco::detectMarkers(imgIn, dict, corners, ids);
        std::vector<cv::Point2f> ptsOut(4);

        int cnt = 0;

        for (int i = 0; i < (int)ids.size(); i++)
        {
            if (ids[i] == 0)
            {
                cnt++;
                int x = (corners[i][0].x + corners[i][2].x) / 2;
                int y = (corners[i][0].y + corners[i][2].y) / 2;

                ptsOut[0] = (cv::Point2f(x, y));
            }
            else if (ids[i] == 34)
            {
                cnt++;

                int x = (corners[i][0].x + corners[i][2].x) / 2;
                int y = (corners[i][0].y + corners[i][2].y) / 2;

                ptsOut[1] = (cv::Point2f(x, y));
            }
            else if (ids[i] == 30)
            {
                cnt++;

                int x = (corners[i][0].x + corners[i][2].x) / 2;
                int y = (corners[i][0].y + corners[i][2].y) / 2;

                ptsOut[2] = (cv::Point2f(x, y));
            }
            else if (ids[i] == 4)
            {
                cnt++;

                int x = (corners[i][0].x + corners[i][2].x) / 2;
                int y = (corners[i][0].y + corners[i][2].y) / 2;

                ptsOut[3] = (cv::Point2f(x, y));
            }
        }

        // Desenha um retângulo usando as coordenadas em ptsOut
        if (cnt == 4)
        {
            cv::line(imgIn, ptsOut[0], ptsOut[1], cv::Scalar(0, 255, 0), 2);
            cv::line(imgIn, ptsOut[1], ptsOut[3], cv::Scalar(0, 255, 0), 2);
            cv::line(imgIn, ptsOut[3], ptsOut[2], cv::Scalar(0, 255, 0), 2);
            cv::line(imgIn, ptsOut[2], ptsOut[0], cv::Scalar(0, 255, 0), 2);
        }

        cv::imshow("Out", imgIn);
        cv::waitKey(1);
    }

    return 0;
}
