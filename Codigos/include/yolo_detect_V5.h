#ifndef YOLO_DETECT_V5_H
#define YOLO_DETECT_V5_H

#include <opencv2/dnn.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <fstream>
#include <thread>
#include <ctime>
#include <iomanip>
#include <sstream>
#include <chrono>
#include <filesystem>
#include <mutex>                 // Inclui funções para exclusão mútua
#include <condition_variable> 
#include <unistd.h> 


void processFrames(cv::dnn::Net& net, const std::vector<std::string>& classNames, cv::Rect& leftROI, cv::Rect& rightROI, int *pipe);
void captureFrames(cv::VideoCapture& cap);
std::string getCurrentDateFormatted(int photoCount);
std::vector<std::string> loadClassNames(const std::string& filename);

#endif